﻿using Autocarrier.Data;
using System.Collections.Generic;
using System.Linq;
using System.Collections;

namespace Autocarrier
{
    class Repository
    {
        private AutocarrierModel model = null;

        public Repository()
        {
            model = new AutocarrierModel();
        }

        public IEnumerable<Driver> GetDrivers()
        {
            return model.Drivers.ToList();
        }

        public IEnumerable<Car> GetCars()
        {
            return model.Cars.ToList();
        }

        public IEnumerable<Trip> GetTrips()
        {
            return model.Trips.ToList();
        }

        public void DeleteDriver(Driver driver)
        {
            model.Drivers.Remove(driver);
            model.SaveChanges();
        }

        public void AddDriver(Driver driver)
        {
            model.Drivers.Add(driver);
            model.SaveChanges();
        }

        public void UpdateDriver(Driver driver)
        {
            model.Entry(driver).State = System.Data.Entity.EntityState.Modified;
            model.SaveChanges();
        }

        public IEnumerable<Driver> SearchDrivers(string search)
        {
            return model.Drivers.Where(x => x.FullName.Contains(search)).ToList();
        }

        public void AddCar(Car car)
        {
            model.Cars.Add(car);
            model.SaveChanges();
        }

        public void UpdateCar(Car car)
        {
            model.Entry(car).State = System.Data.Entity.EntityState.Modified;
            model.SaveChanges();
        }

        public void DeleteCar(Car car)
        {
            model.Cars.Remove(car);
            model.SaveChanges();
        }

        public void AddTrip(Trip trip)
        {
            model.Trips.Add(trip);
            model.SaveChanges();
        }

        public void UpdateTrip(Trip trip)
        {
            model.Entry(trip).State = System.Data.Entity.EntityState.Modified;
            model.SaveChanges();
        }

        public void DeleteTrip(Trip trip)
        {
            model.Trips.Remove(trip);
            model.SaveChanges();
        }

        public IEnumerable SearchCars(string search)
        {
            return model.Cars.Where(x => x.Mark.Contains(search)).ToList();
        }

        public IEnumerable SearchTrips(string fromWhere)
        {
            return model.Trips.Where(x => x.FromWhere.Contains(fromWhere)).ToList();
        }
    }
}