﻿using Autocarrier.Data;
using System;
using System.Windows;
using System.Windows.Controls;

namespace Autocarrier
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Repository repository = null;

        private bool driverWasAdded;
        private bool driverWasChanged;

        private bool carWasAdded;
        private bool carWasChanged;

        private bool tripWasAdded;
        private bool tripWasChanged;

        public MainWindow()
        {
            InitializeComponent();

            repository = new Repository();
            Loaded += MainWindow_Loaded;

            dataGridDrivers.CurrentCellChanged += DataGridDrivers_CurrentCellChanged;
            dataGridDrivers.RowEditEnding += DataGridDrivers_RowEditEnding;
            dataGridDrivers.AddingNewItem += DataGridDrivers_AddingNewItem;

            dataGridCars.CurrentCellChanged += DataGridCars_CurrentCellChanged;
            dataGridCars.RowEditEnding += DataGridCars_RowEditEnding;
            dataGridCars.AddingNewItem += DataGridCars_AddingNewItem;

            dataGridTrips.CurrentCellChanged += DataGridTrips_CurrentCellChanged;
            dataGridTrips.RowEditEnding += DataGridTrips_RowEditEnding;
            dataGridTrips.AddingNewItem += DataGridTrips_AddingNewItem;
        }

        private void DataGridTrips_AddingNewItem(object sender, AddingNewItemEventArgs e)
        {
            tripWasAdded = true;
        }

        private void DataGridTrips_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            tripWasChanged = true;
        }

        private void DataGridTrips_CurrentCellChanged(object sender, EventArgs e)
        {
            if (tripWasChanged)
            {
                Trip trip = GetCurrentTrip();

                if (tripWasAdded) repository.AddTrip(trip);

                else repository.UpdateTrip(trip);

                tripWasChanged = false;
                tripWasAdded = false;
            }
        }
        
        private void DataGridCars_AddingNewItem(object sender, AddingNewItemEventArgs e)
        {
            carWasAdded = true;
        }

        private void DataGridCars_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            carWasChanged = true;
        }

        private void DataGridCars_CurrentCellChanged(object sender, EventArgs e)
        {
            if (carWasChanged)
            {
                Car car = GetCurrentCar();

                if (carWasAdded) repository.AddCar(car);

                else repository.UpdateCar(car);

                carWasChanged = false;
                carWasAdded = false;
            }
        }

        private Driver GetCurrentDriver()
        {
            return dataGridDrivers.SelectedItem as Driver;
        }

        private Car GetCurrentCar()
        {
            return dataGridCars.SelectedItem as Car;
        }

        private Trip GetCurrentTrip()
        {
            return dataGridTrips.SelectedItem as Trip;
        }

        private void DataGridDrivers_AddingNewItem(object sender, AddingNewItemEventArgs e)
        {
            driverWasAdded = true;
        }

        private void DataGridDrivers_CurrentCellChanged(object sender, EventArgs e)
        {
            if (driverWasChanged)
            {
                Driver driver = GetCurrentDriver();

                if (driverWasAdded) repository.AddDriver(driver);

                else repository.UpdateDriver(driver);

                driverWasChanged = false;
                driverWasAdded = false;
            }
        }

        private void DataGridDrivers_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            driverWasChanged = true;
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            dataGridCars.ItemsSource = repository.GetCars();
            dataGridTrips.ItemsSource = repository.GetTrips();
            dataGridDrivers.ItemsSource = repository.GetDrivers();
        }

        private void ButtonDeleteTrips_Click(object sender, RoutedEventArgs e)
        {
            Trip trip = GetCurrentTrip();

            if (trip == null)
                return;

            repository.DeleteTrip(trip);
            LoadData();
        }

        private void ButtonDeleteDriver_Click(object sender, RoutedEventArgs e)
        {
            Driver driver = GetCurrentDriver();

            if (driver == null)
                return;

            repository.DeleteDriver(driver);
            LoadData();
        }

        private void textBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            var search = this.textBox.Text;

            if (string.IsNullOrEmpty(search))
                dataGridDrivers.ItemsSource = repository.GetDrivers();

            else dataGridDrivers.ItemsSource = repository.SearchDrivers(search);
        }

        private void ButtonDeleteCar_Click(object sender, RoutedEventArgs e)
        {
            Car car = GetCurrentCar();

            if (car == null)
                return;

            repository.DeleteCar(car);
            LoadData();
        }

        private void textBox1_TextChanged(object sender, TextChangedEventArgs e)
        {
            var search = this.textBox1.Text;

            if (string.IsNullOrEmpty(search))
                dataGridCars.ItemsSource = repository.GetCars();

            else dataGridCars.ItemsSource = repository.SearchCars(search);
        }

        private void textBox2_TextChanged(object sender, TextChangedEventArgs e)
        {
            var search = this.textBox2.Text;

            if (string.IsNullOrEmpty(search))
                dataGridTrips.ItemsSource = repository.GetTrips();

            else dataGridTrips.ItemsSource = repository.SearchTrips(search);
        }
    }
}
/*Необходимо реализовать программу для автоперевозчиков. В ней должен быть интерфейс для добавления, 
поиска, изменения и удаления информации о: 
1) автомобиле: марка, год выпуска, пробег, стоимость, тонаж, номер.
2) водителе: ФИО, стаж, оклад, место проживания, телефон.
3) рейсах: от куда, куда, водитель, машина, груз и время перевозки. 
Предусмотреть грамотную нормализацию и связи бд и таблиц.
Обязательно использовать: Entity Model-First, LINQ, нормальный интерфейс, комментарии XML,
обработка исключений, иначе будет соответственная оценка.*/