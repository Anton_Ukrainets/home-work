﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Forms;
using System.IO;
using System.Security.Cryptography;

namespace SearchFile
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void OpenButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                FolderBrowserDialog fbd = new FolderBrowserDialog();
                if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    this.TbPath.Text = fbd.SelectedPath;
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }

        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string searchPath = this.TbPath.Text;
                string searchPattern = this.TbMask.Text;

                FilesSearcher filesSearcher = new FilesSearcher();

                var files = filesSearcher.Search(searchPath, searchPattern);

                FilesRepository filesRepository = new FilesRepository();

                filesRepository.Clear();

                filesRepository.AddRange(files);
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }                       
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string searchFileName = this.TbFileName.Text;
                int fileSize = int.Parse(this.TbSize.Text);
                string content = this.TbContent.Text;
                DateTime lastAccess = DateTime.Parse(this.TbLastWrite.Text);

                FilesRepository filesRepository = new FilesRepository();

                var foundFiles = filesRepository.SearchBy(searchFileName, fileSize, content, lastAccess);

                this.dataGrid.ItemsSource = foundFiles;
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }            
        }
    }
}
/*Написать программу:
1) Программа должна иметь возможность предоставить пользователю указать диск или каталог, 
и маску файлов, которые необходимо найти и ихнее содержимое сохранить в БД.
2) Пользователь может ввести в программе критерии поиска: Имя_файла, размер, дата_изменения и текст, 
который необходимо найти и получить результат в удобно читаемом виде.
3) Пользователь должен иметь возможность синхронизировать записи в базе данных с файлами, 
которые были со временем изменены, перемещены или удалены.
Обязательно: LINQ, Entity, интуитивно понятный, удобный, красивый интерфейс, XML документация, 
обработка ошибок и проверки.
Остальное на усмотрение разработчика.*/