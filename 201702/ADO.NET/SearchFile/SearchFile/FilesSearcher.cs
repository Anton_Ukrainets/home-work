﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Security.Cryptography;

namespace SearchFile
{
    public class FilesSearcher
    {
        public IEnumerable<File> Search(string searchPath, string searchPattern)
        {
            var files =
                from file in Directory.EnumerateFiles(searchPath, searchPattern, SearchOption.AllDirectories)
                let fileInfo = new FileInfo(file)
                select new File
                {
                    path = file,
                    size = fileInfo.Length,
                    date = fileInfo.LastWriteTime,
                    Content = System.IO.File.ReadAllText(file),
                    MD5 = GetFileHash(file)
                };

            return files;
        }

        private string GetFileHash(string path)
        {
            string str = string.Empty;
            try
            {
                using (FileStream fs = System.IO.File.OpenRead(path))
                {
                    byte[] buf = MD5.Create().ComputeHash(fs);

                    str = BitConverter.ToString(buf).Replace("-", "");
                }
            }
            catch (Exception)
            {
                str = "Error hash";
            }
            return str;
        }
    }
}