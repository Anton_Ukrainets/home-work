﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SearchFile
{
    public class FilesRepository
    {
        FileSystemPCEntities domainModel = null;

        public FilesRepository()
        {
            domainModel = new FileSystemPCEntities();
        }

        public void AddRange(IEnumerable<File> files)
        {
            domainModel.Files.AddRange(files);
            domainModel.SaveChanges();
        }

        public IEnumerable<File> SearchBy(string searchFileName, int fileSize, string content, DateTime lastAccess)
        {
            var files = 
                domainModel.Files.ToArray()
                .Where(file =>
                    Path.GetFileName(file.path) == searchFileName
                    || file.size == fileSize
                    || file.Content.Contains(content)
                    || file.date == lastAccess);

            return files;
        }

        public void Clear()
        {
            domainModel.Files.RemoveRange(domainModel.Files.ToArray());
            domainModel.SaveChanges();
        }
    }
}