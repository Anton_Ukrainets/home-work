﻿using System.Collections.Generic;
using System.IO;

namespace SearchingFiles
{
    public class DriveHelper
    {
        private IEnumerable<DriveInfo> GetDrives()
        {
            return DriveInfo.GetDrives();
        }

        public string GetUSBDrivePath()
        {
            string driveType = string.Empty;

            foreach (var drive in GetDrives())
                if (drive.DriveType == DriveType.Removable)
                    driveType = drive.Name;

            return driveType;
        } 
    }
}