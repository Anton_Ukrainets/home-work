﻿using System;
using System.Data.SqlClient;
using System.IO;

namespace SearchingFiles
{
    public class SystemWatcher
    {
        private string connectionString = null;        

        public SystemWatcher(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void Run(string directoryPath)
        {
            FileSystemWatcher watcher = new FileSystemWatcher();
            watcher.Path = directoryPath;

            watcher.NotifyFilter =
                NotifyFilters.LastAccess
              | NotifyFilters.LastWrite
              | NotifyFilters.FileName
              | NotifyFilters.DirectoryName
              | NotifyFilters.FileName
              | NotifyFilters.Size;

            watcher.Filter = "*.*";

            watcher.Changed += new FileSystemEventHandler(OnChanged);
            watcher.Created += new FileSystemEventHandler(OnCreated);
            watcher.Deleted += new FileSystemEventHandler(onDeleted);
            watcher.Renamed += new RenamedEventHandler(OnRenamed);

            watcher.IncludeSubdirectories = true;
            watcher.EnableRaisingEvents = true;
        }

        private void OnChanged(object source, FileSystemEventArgs e)
        {
            if (e.ChangeType != WatcherChangeTypes.Changed)
                return;

            try
            {
                UpdateFilesMetadata(e);
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
        }

        private void OnRenamed(object source, RenamedEventArgs e)
        {
            if (e.ChangeType != WatcherChangeTypes.Renamed)
                return;

            try
            {
                RenameFilesIntoDB(e);
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
        }

        private void OnCreated(object source, FileSystemEventArgs e)
        {
            if (e.ChangeType != WatcherChangeTypes.Created)
                return;

            try
            {
                CreatedFilesMetadata(e);
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }            
        }

        private void onDeleted(object source, FileSystemEventArgs e)
        {
            if (e.ChangeType != WatcherChangeTypes.Deleted)
                return;

            try
            {
                DeleteFilesIntoDB(e);
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
        }

        private void DeleteFilesIntoDB(FileSystemEventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                FileInfo file = new FileInfo(e.FullPath);
                FileHelper fileHelper = new FileHelper();

                SqlCommand command = connection.CreateCommand();

                command.Parameters.Add(new SqlParameter("path", file.FullName));

                command.CommandText =
                    "DELETE FROM Files WHERE path = @path";

                command.ExecuteNonQuery();
            }
        }

        private void CreatedFilesMetadata(FileSystemEventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                FileInfo file = new FileInfo(e.FullPath);
                FileHelper fileHelper = new FileHelper();

                SqlCommand command = connection.CreateCommand();

                command.Parameters.Add(new SqlParameter("path", file.FullName));
                command.Parameters.Add(new SqlParameter("size", file.Length));
                command.Parameters.Add(new SqlParameter("date", file.LastWriteTime));
                command.Parameters.Add(new SqlParameter("MD5", fileHelper.GetMD5Hash(e.FullPath)));

                command.CommandText =
                    "INSERT INTO Files (path, size, date, MD5) VALUES(@path, @size, @date, @MD5)";

                command.ExecuteNonQuery();
            }
        }

        private void UpdateFilesMetadata(FileSystemEventArgs e)
        {
            if (e.ChangeType == WatcherChangeTypes.Changed 
                && Directory.Exists(e.FullPath))
                return;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                FileInfo file = new FileInfo(e.FullPath);
                FileHelper fileHelper = new FileHelper();

                SqlCommand command = connection.CreateCommand();

                command.Parameters.Add(new SqlParameter("path", file.FullName));
                command.Parameters.Add(new SqlParameter("size", file.Length));
                command.Parameters.Add(new SqlParameter("date", file.LastWriteTime));
                command.Parameters.Add(new SqlParameter("MD5", fileHelper.GetMD5Hash(e.FullPath)));

                command.CommandText =
                    "UPDATE Files SET size = @size, date = @date, MD5 = @MD5 WHERE path = @path";

                command.ExecuteNonQuery();
            }
        }

        private void RenameFilesIntoDB(RenamedEventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();

                command.Parameters.Add(new SqlParameter("newPath", e.FullPath));
                command.Parameters.Add(new SqlParameter("oldPath", e.OldFullPath));

                command.CommandText =
                    "UPDATE Files SET path = @newPath WHERE path = @oldPath";

                command.ExecuteNonQuery();
            }
        }
    }
}