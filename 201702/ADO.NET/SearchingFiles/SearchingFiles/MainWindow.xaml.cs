﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Windows;
using System.Windows.Controls;
using System.Management;
using System.Management.Instrumentation;
using System.Windows.Forms;

namespace SearchingFiles
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //Tray
        private NotifyIcon TrayIcon = null;
        private System.Windows.Controls.ContextMenu TrayMenu = null;
        private WindowState fCurrentWindowState = WindowState.Normal;
        private bool fCanClose = false;
        //Tray

        private string connectionString = null;
        private string directoryPath = null;
        private SystemWatcher watcher = null;

        public MainWindow()
        {
            InitializeComponent();

            connectionString =
            @"Data Source=server2;Initial Catalog=FileSystemPC27;Integrated Security=True;";

            directoryPath = @"D:\";

            watcher = new SystemWatcher(connectionString);
            watcher.Run(directoryPath);

            FindNewDuplicates();
        }

        private void FindNewDuplicates()
        {
            DriveHelper drive = new DriveHelper();
            string usbPath = drive.GetUSBDrivePath();

            FileHelper fileHelper = new FileHelper();
            fileHelper.EqualsFiles(usbPath, this.connectionString);            
        }

        private void InsertFilesData(
            IEnumerable<FileDescriptor> fileDescriptors)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    foreach (var item in fileDescriptors)
                    {
                        SqlCommand command = connection.CreateCommand();

                        command.Parameters.Add(new SqlParameter("path", item.Path));
                        command.Parameters.Add(new SqlParameter("size", item.Size));
                        command.Parameters.Add(new SqlParameter("date", item.Date));
                        command.Parameters.Add(new SqlParameter("MD5", item.MD5Hash));

                        command.CommandText =
                            "INSERT INTO Files (path, size, date, MD5) VALUES(@path, @size, @date, @MD5)";


                        command.Connection = connection;

                        command.ExecuteNonQuery();
                    }

                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                FolderBrowserDialog fbd = new FolderBrowserDialog();
                if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    directoryPath = fbd.SelectedPath;

                FileHelper fileHelper = new FileHelper();

                IEnumerable<FileDescriptor> filesDescriptors = fileHelper.GetFilesDescriptors(directoryPath);
                InsertFilesData(filesDescriptors);
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
        }

        //Tray
        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);
            createTrayIcon();

            this.WindowState = WindowState.Minimized;
            this.Visibility = Visibility.Hidden;
        }

        private bool createTrayIcon()
        {
            bool result = false;
            if (TrayIcon == null)
            {
                TrayIcon = new NotifyIcon();
                TrayIcon.Icon = new System.Drawing.Icon("setup.ico");

                TrayMenu = Resources["TrayMenu"] as System.Windows.Controls.ContextMenu;

                TrayIcon.Click += delegate (object sender, EventArgs e)
                {
                    if ((e as MouseEventArgs).Button
                    == MouseButtons.Left)
                        ShowHideMainWindow(sender, null);
                    else
                    {
                        TrayMenu.IsOpen = true;
                        Activate();
                    }
                };

                result = true;
            }
            else result = true;

            TrayIcon.Visible = true;

            return result;
        }

        private void ShowHideMainWindow(object sender, RoutedEventArgs e)
        {
            TrayMenu.IsOpen = false;
            if (IsVisible)
            {
                Hide();

                (TrayMenu.Items[0] as System.Windows.Controls.MenuItem).Header = "Show";
            }
            else
            {
                Show();

                (TrayMenu.Items[0] as System.Windows.Controls.MenuItem).Header = "Hide";
                WindowState = CurrentWindowState;
                Activate();
            }
        }
        public WindowState CurrentWindowState
        {
            get { return fCurrentWindowState; }
            set { fCurrentWindowState = value; }
        }

        protected override void OnStateChanged(EventArgs e)
        {
            base.OnStateChanged(e);
            if (this.WindowState == WindowState.Minimized)
            {
                Hide();

                (TrayMenu.Items[0] as System.Windows.Controls.MenuItem).Header = "Show";
            }

            else CurrentWindowState = WindowState;
        }

        public bool CanClose
        {
            get { return fCanClose; }
            set { fCanClose = value; }
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);
            if (!CanClose)
            {
                e.Cancel = true;

                CurrentWindowState = this.WindowState;

                (TrayMenu.Items[0] as System.Windows.Controls.MenuItem).Header = "Show";

                Hide();
            }
            else
            {
                TrayIcon.Visible = false;
            }
        }

        private void MenuExitClick(object sender, RoutedEventArgs e)
        {
            CanClose = true;

            this.Close();
        }
    }
}
/*Первую ДЗ, в которой было необходимо сохранять список файлов в бд необходимо апгрейдить. 
Апгрейд заключается в следующем:
1) Программа должна при запуске сворачиваться в трей.
2) При подключении флешки автоматически искать все файлы. 
Если на флешке найдены файлы, которые есть на hdd, 
программа должна предложить удалить дубликаты с флешки. 
Если найдены файлы, которых нет на hdd, тогда предложить скопировать в указанное пользователем место.
Вся информация о файлах должна находиться в базе данных.
Пользователь должен иметь возможность указать в настройках, какие флешки игнорировать.
Информацию о настройках программы необходимо хранить в базе данных.
Обязательное: XML документация и проверки.

PS. Вспоминайте WinForms, там было у подобное задание с флешками.*/