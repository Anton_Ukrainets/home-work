﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SearchingFiles
{
    public class FileDescriptor
    {
        public string Path { get; set; }

        public string Size { get; set; }

        public DateTime Date { get; set; }

        public string MD5Hash { get; set; }
    }
}