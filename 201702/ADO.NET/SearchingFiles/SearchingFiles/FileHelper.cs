﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography;
using System.Windows;
using System.Windows.Forms;

namespace SearchingFiles
{
    public class FileHelper
    {
        public IEnumerable<FileInfo> SearchingFiles(string directoryPath)
        {
            DirectoryInfo dirInfo = new DirectoryInfo(directoryPath);
            List<FileInfo> files = new List<FileInfo>();
            DriveHelper helper = new DriveHelper();

            try
            {
                files.AddRange(dirInfo.GetFiles());

                foreach (var file in dirInfo.GetDirectories())
                {
                    string usbPath = helper.GetUSBDrivePath() + "System Volume Information";

                    if (file.FullName != usbPath)
                    {
                        FileAttributes fileAtt = File.GetAttributes(file.FullName);
                        if (fileAtt != FileAttributes.System
                            || fileAtt != FileAttributes.Hidden
                            || fileAtt != FileAttributes.Temporary)
                        {
                            var dirFiles = SearchingFiles(file.FullName);

                            files.AddRange(dirFiles);
                        }
                    }                   
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }

            return files;
        }

        public IEnumerable<FileDescriptor> GetFilesDescriptors(string directoryPath)
        {
            List<FileDescriptor> fileDescriptors = new List<FileDescriptor>();

            var files = SearchingFiles(directoryPath);

            foreach (var file in files)
            {
                FileAttributes att = File.GetAttributes(file.FullName);

                if (att != FileAttributes.System)
                {
                    var md5 = GetMD5Hash(file.FullName);

                    if (md5 == null)
                        continue;

                    FileDescriptor item = new FileDescriptor()
                    {
                        Path = file.FullName,
                        Size = file.Length.ToString(),
                        Date = file.LastWriteTime,
                        MD5Hash = md5,
                    };

                    fileDescriptors.Add(item);
                }
            }

            return fileDescriptors;
        }

        public string GetMD5Hash(string filePath)
        {
            try
            {
                using (var md5 = MD5.Create())
                {
                    using (var stream = File.OpenRead(filePath))
                    {
                        byte[] hash = md5.ComputeHash(stream);
                        return ByteArrayToString(hash);
                    }
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }

            return null;
        }

        private static string ByteArrayToString(byte[] ba)
        {
            string hex = BitConverter.ToString(ba);
            return hex.Replace("-", "");
        }

        private void DeleteFiles(List<string> deletedFiles)
        {
            if (System.Windows.MessageBox.Show
                        (
                        "Удалить дубликаты с флешки?",
                        "Удалить дубликаты",
                        MessageBoxButton.YesNo,
                        MessageBoxImage.Warning
                        )
                        == MessageBoxResult.Yes)
            {
                foreach (string file in deletedFiles)
                    File.Delete(file);
            }
        }

        private void CopyFiles(List<string> copiedFiles)
        {
            if (System.Windows.MessageBox.Show
                        (
                        "Скопировать файлы с флешки?",
                        "Копирование файлов",
                        MessageBoxButton.YesNo,
                        MessageBoxImage.Warning
                        )
                        == MessageBoxResult.Yes)
            {
                FolderBrowserDialog fbd = new FolderBrowserDialog();
                if (fbd.ShowDialog() == DialogResult.OK)
                    foreach (string file in copiedFiles)
                        File.Copy(Path.GetFullPath(file), fbd.SelectedPath + @"\" + Path.GetFileName(file), true);
            }
        }

        public void EqualsFiles(string path, string connectionString)
        {
            FileHelper w = new FileHelper();
            List<string> newFiles = new List<string>();
            List<string> oldFiles = new List<string>();
            List<string> hashFiles = new List<string>();

            using (SqlConnection connect = new SqlConnection(connectionString))
            {
                connect.Open();

                SqlCommand command = connect.CreateCommand();

                command.CommandText = "SELECT MD5 FROM Files";

                var reader = command.ExecuteReader();
                while (reader.Read())
                    hashFiles.Add(reader.GetString(0));

                IEnumerable<FileInfo> files = w.SearchingFiles(path);

                foreach (var file in files)
                {
                    {
                        if (!hashFiles.Exists(x => x == w.GetMD5Hash(file.FullName)))
                            newFiles.Add(file.FullName);

                        else
                            oldFiles.Add(file.FullName);
                    }
                }

                if (oldFiles.Count != 0)
                    DeleteFiles(oldFiles);

                if (newFiles.Count != 0)
                    CopyFiles(newFiles);
            }
        }
    }
}