﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Threading;

namespace CopyFileThread
{
    public partial class Form1 : Form
    {
        private FileHelper fileHelper = null;
        private Thread copyThread = null;
        private ManualResetEvent pauseResetEvent = null;

        public Form1()
        {
            InitializeComponent();

            fileHelper = new FileHelper();
            pauseResetEvent = new ManualResetEvent(false);

            this.PauseButton.Enabled = false;
            this.AbortButton.Enabled = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                string sourcePath = textBox1.Text;
                string targetPath = textBox2.Text;

                Action<int> updateProgressBar = progress => Invoke(new Action(() => this.progressBar1.Value = progress));

                copyThread = new Thread(x => CopyAsync(x, sourcePath, targetPath, updateProgressBar));
                copyThread.Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void OpenButton_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog ofd = new OpenFileDialog();
                if (ofd.ShowDialog() == DialogResult.OK)
                    this.textBox1.Text = ofd.FileName;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            try
            {
                FolderBrowserDialog fbd = new FolderBrowserDialog();
                if (fbd.ShowDialog() == DialogResult.OK)
                    this.textBox2.Text = fbd.SelectedPath;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void PauseButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.PauseButton.Text = this.PauseButton.Text == "Continue" ? "Pause" : "Continue";
                pauseResetEvent.Set();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void AbortButton_Click(object sender, EventArgs e)
        {
            try
            {
                copyThread.Abort();

                this.AbortButton.Enabled = false;
                this.CopyButton.Enabled = false;
                this.PauseButton.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }            
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                this.AbortButton_Click(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void CopyAsync(
            object state,
            string sourcePath,
            string targetPath,
            Action<int> progressCallback)
        {
            try
            {
                Invoke((Action)(() =>
                {
                    this.CopyButton.Enabled = false;

                    this.PauseButton.Enabled = true;
                    this.AbortButton.Enabled = true;
                }));

                fileHelper.CopyFile(sourcePath, targetPath, progressCallback, pauseResetEvent);

                Invoke((Action)(() =>
                {
                    this.CopyButton.Enabled = true;

                    this.PauseButton.Enabled = false;
                    this.AbortButton.Enabled = false;
                }));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }            
        }
    }
}
/*Написать программу, в которой пользователь должен указать путь к файлу, 
который необходимо скопировать и место, куда необходимо скопировать. 
Программа должна копировать файл в одельном потоке( класс Thread ) при нажатии на кнопку Copy. 
Пользователь должен иметь возможность приостановить копирование с помощью кнопки Pause, 
и отменить копирование с помощью кнопки Abort. Прогресс копирования должен отображаться в ProgressBar. 
В момент копирования кнопка Copy должна быть недоступная для нажатия, 
а после копирования или после нажатия на Abort снова становиться доступной. 
Кнопки Abort и Pause должны быть доступные только в момент копирования.
Программа должна адекватно реагировать на закрытие окна в момент копирования файла.
Обязательно соблюдать все правила кодирования.*/