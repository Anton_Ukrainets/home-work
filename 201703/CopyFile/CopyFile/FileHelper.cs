﻿using System;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace CopyFileThread
{
    class FileHelper
    {
        private string ReturnFileName(string sourcePath)
        {
            DirectoryInfo dirInfo = new DirectoryInfo(sourcePath);
            sourcePath = dirInfo.Name;

            return sourcePath;
        }

        public void CopyFile(
            string sourcePath,
            string targetPath,
            Action<int> progressAction,
            ManualResetEvent pauseResetEvent)
        {
            try
            {
                string destFile = Path.Combine(targetPath, ReturnFileName(sourcePath));

                using (FileStream sourceFileStream = File.OpenRead(sourcePath))
                {
                    using (FileStream targetFileStream = File.OpenWrite(destFile))
                    {
                        byte[] buffer = new byte[10 * 1024];

                        int n = 0;
                        int progress = 0;

                        do
                        {
                            bool shouldPause = pauseResetEvent.WaitOne(0);
                            if (shouldPause)
                            {
                                pauseResetEvent.Reset();
                                pauseResetEvent.WaitOne();
                                pauseResetEvent.Reset();
                            }

                            n = sourceFileStream.Read(buffer, 0, buffer.Length);
                            targetFileStream.Write(buffer, 0, n);

                            progress = (int)(sourceFileStream.Position * 100 / sourceFileStream.Length);
                            progressAction(progress);
                        }
                        while (n > 0);
                    }
                }
            }
            catch (ThreadAbortException)
            {
                return;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}