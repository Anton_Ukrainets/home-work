﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Threading;
using System.Windows.Forms;

namespace TaskManagerWPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //Tray
        private NotifyIcon TrayIcon = null;
        private System.Windows.Controls.ContextMenu TrayMenu = null;
        private WindowState fCurrentWindowState = WindowState.Normal;
        private bool fCanClose = false;
        //Tray

        private List<ProcessInfo> blackList = null;
        private ProcessInfo item = null;
        private System.Threading.Timer timer;

        public MainWindow()
        {
            InitializeComponent();

            item = new ProcessInfo();
            blackList = new List<ProcessInfo>();

            RefreshProcessesList();
            WatchProcesses();
        }

        private void WatchProcesses()
        {
            timer = new System.Threading.Timer(
                obj =>
                {
                    var names = blackList
                        .Select(x => x.Name)
                        .ToArray();

                    KillProcessByNames(names);

                    System.Windows.Application.Current.Dispatcher.Invoke(() => RefreshProcessesList());
                },
                null,
                5000,
                5000);
        }

        private void RefreshProcessesList()
        {
            this.listView.ItemsSource = GetProcessInfo();
        }

        private IEnumerable<ProcessInfo> GetProcessInfo()
        {
            Process[] processes = Process.GetProcesses();
            List<ProcessInfo> processList = new List<ProcessInfo>();

            foreach (var process in processes)
            {

                processList.Add(
                    new ProcessInfo()
                    {
                        Name = process.ProcessName,
                        Id = process.Id,
                        State = process.Responding ? "Running" : "Not Running",
                        Username = Environment.UserName,
                        Memory = process.WorkingSet64 / 1024 / 1024
                    });
            }

            return processList.OrderBy(x => x.Name).ToArray();
        }

        private void KillButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                foreach (Process proc in Process.GetProcesses())
                {
                    ProcessInfo item = this.listView.SelectedItem as ProcessInfo;

                    if (proc.Id == item.Id)
                    {
                        proc.Kill();
                        RefreshProcessesList();

                        break;
                    }
                }

            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
        }

        private void OpenButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Microsoft.Win32.OpenFileDialog ofd = new Microsoft.Win32.OpenFileDialog();
                if (ofd.ShowDialog() == true)
                    Process.Start(ofd.FileName);

            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
        }

        private void AddToBlackListButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                foreach (Process proc in Process.GetProcesses())
                {
                    ProcessInfo item = this.listView.SelectedItem as ProcessInfo;

                    if (item == null)
                        return;

                    blackList.Add(item);

                    ListBoxItem lbi = new ListBoxItem();

                    if (proc.Id == item.Id)
                    {
                        lbi.Content = proc.ProcessName;

                        this.listBox.Items.Add(lbi);
                    }
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
        }

        private void KillProcessByNames(string[] names)
        {
            foreach (Process proc in Process.GetProcesses())
            {
                if (names.Contains(proc.ProcessName))
                {
                    proc.Kill();
                }
            }
        }

        //Tray
        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);
            createTrayIcon();

            this.WindowState = WindowState.Minimized;
            this.Visibility = Visibility.Hidden;
        }

        private bool createTrayIcon()
        {
            bool result = false;
            if (TrayIcon == null)
            {
                TrayIcon = new NotifyIcon();
                TrayIcon.Icon = new System.Drawing.Icon("setup.ico");

                TrayMenu = Resources["TrayMenu"] as System.Windows.Controls.ContextMenu;

                TrayIcon.Click += delegate (object sender, EventArgs e)
                {
                    if ((e as MouseEventArgs).Button
                    == MouseButtons.Left)
                        ShowHideMainWindow(sender, null);
                    else
                    {
                        TrayMenu.IsOpen = true;
                        Activate();
                    }
                };

                result = true;
            }
            else result = true;

            TrayIcon.Visible = true;

            return result;
        }

        private void ShowHideMainWindow(object sender, RoutedEventArgs e)
        {
            TrayMenu.IsOpen = false;
            if (IsVisible)
            {
                Hide();

                (TrayMenu.Items[0] as System.Windows.Controls.MenuItem).Header = "Show";
            }
            else
            {
                Show();

                (TrayMenu.Items[0] as System.Windows.Controls.MenuItem).Header = "Hide";
                WindowState = CurrentWindowState;
                Activate();
            }
        }
        public WindowState CurrentWindowState
        {
            get { return fCurrentWindowState; }
            set { fCurrentWindowState = value; }
        }

        protected override void OnStateChanged(EventArgs e)
        {
            base.OnStateChanged(e);
            if (this.WindowState == WindowState.Minimized)
            {
                Hide();

                (TrayMenu.Items[0] as System.Windows.Controls.MenuItem).Header = "Show";
            }

            else CurrentWindowState = WindowState;
        }

        public bool CanClose
        {
            get { return fCanClose; }
            set { fCanClose = value; }
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);
            if (!CanClose)
            {
                e.Cancel = true;

                CurrentWindowState = this.WindowState;

                (TrayMenu.Items[0] as System.Windows.Controls.MenuItem).Header = "Show";

                Hide();
            }
            else
            {
                TrayIcon.Visible = false;
            }
        }

        private void MenuExitClick(object sender, RoutedEventArgs e)
        {
            CanClose = true;

            this.Close();
        }
    }
}
/*Написать программу, менеджер процессов, который должен:
1) Отображать максимально подробную информацию о процессах
(смотреть все свойства класса Process в конспекте)
2) Настройки, с помощью которых пользователь может указать процессы, 
которые необходимо всегда завершать. 
При завершении должно выводиться сообщение в правом нижнем углу монитора.
3) Запускать программу
4) Завершать программу
5) Сворачиваться в трей
Должен быть красивый, интуитивно понятный интерфейс.
Предусмотреть на будущее в своей прогамме очистку диска от мусора, 
редактор реестра и оптимизатор реестра, это у нас будет в будущем.
Обязательно соблюдать все правила кодирования.*/