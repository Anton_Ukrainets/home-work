﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegistryEditor
{
    public enum TypeAction
    {
        CreateFolder,
        ChangeSelection,

        CreateString,
        CreateBinary,
        CreateDWord,
        CreateQWord,
        CreateMultiString,
        CreateExpandString
    }
}