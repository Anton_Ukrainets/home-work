﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Socket socket = null;
        private Task listenToMessages = null;

        public MainWindow()
        {
            InitializeComponent();

            //socket = GetSocket();

            //listenToMessages = ListenToMessageAsync();

            this.SendButton.IsEnabled = false;
        }

        private string ListenToMessage()
        {
            byte[] showBuf = new byte[4096];

            int receviceBytes = socket.Receive(showBuf);

            string receivedMessage = Encoding.UTF8.GetString(showBuf, 0, receviceBytes);

            return receivedMessage;
        }

        private Task ListenToMessageAsync()
        {
            return Task.Run(() => ListenToMessages());
        }

        private void ListenToMessages()
        {
            while (true)
            {
                try
                {
                    string message = ListenToMessage();

                    this.tbShowMsg.Dispatcher.Invoke(new Action(
                        () => this.tbShowMsg.Text += "Sended: " + message + "\r\n"));
                }
                catch
                {
                   
                }
            }
        }

        private void SendMessage(string message)
        {
            try
            {
                if (this.socket == null)
                    return;

                byte[] sendBuf = Encoding.UTF8.GetBytes(message);

                socket.Send(sendBuf);
            }
            catch(SocketException ex)
            {
                MessageBox.Show(ex.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private async void SendButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string message = this.TbSendMsg.Text;

                this.TbSendMsg.Text = string.Empty;

                await Task.Run(()=>SendMessage(message));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private Socket GetSocket()
        {
            try
            {
                string hostName = this.TbIdNamePc.Text;
                int port = int.Parse(this.TbPort.Text);

                IPHostEntry ipHost = Dns.GetHostEntry(hostName);
                IPAddress ipAddr = ipHost.AddressList[0];
                IPEndPoint ipEndPoint = new IPEndPoint(ipAddr, port);

                Socket socket =
                    new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

                socket.Connect(ipEndPoint);

                MessageBox.Show("Youre connected.");

                return socket;
            }
            catch (SocketException)
            {
                MessageBox.Show("Server doesnt work");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return null;
        }

        private void ConnectButton_Click(object sender, RoutedEventArgs e)
        {
            this.SendButton.IsEnabled = true;

            socket = GetSocket();

            listenToMessages = ListenToMessageAsync();
        }

        private void DisconnectButton_Click(object sender, RoutedEventArgs e)
        {
            this.socket.Dispose();

            this.SendButton.IsEnabled = false;
        }
    }
}