﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.Threading;

namespace Server
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string hostName = null;

        private IPHostEntry ipHost = null;
        private IPAddress ipAddr = null;
        private IPEndPoint ipEndPoint = null;
        private List<Socket> connections = null;

        private Socket socket;

        private List<Task> listenToTasks = null;
        private Task acceptConnectionsTask = null;

        public MainWindow()
        {
            InitializeComponent();

            hostName = Dns.GetHostName();

            ipHost = Dns.GetHostEntry(hostName);
            ipAddr = ipHost.AddressList[0];
            ipEndPoint = new IPEndPoint(ipAddr, 33333);

            connections = new List<Socket>();
            listenToTasks = new List<Task>();

            InitSocket();
            acceptConnectionsTask = AcceptConnectionsAsync();
        }

        private void InitSocket()
        {
            if (socket != null)
                return;

            socket = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

            socket.Bind(ipEndPoint);
            socket.Listen(10);
        }

        private void AcceptConnections()
        {
            while (true)
            {
                Socket newConnection = socket.Accept();
                connections.Add(newConnection);

                Task listenToTask = ListenToAsync(newConnection);
                listenToTasks.Add(listenToTask);
            }
        }

        private Task AcceptConnectionsAsync()
        {
            return Task.Run(() => AcceptConnections());
        }

        private void SendMessage(string message)
        {
            byte[] receiveBuf = Encoding.UTF8.GetBytes(message);

            foreach (var connection in connections.ToList())
            {
                if (!connection.Connected)
                {
                    connections.Remove(connection);
                }
                else
                {
                    connection.Send(receiveBuf, SocketFlags.None);
                }
            }
        }

        private string ListenToMessage(Socket connection)
        {
            try
            {
                byte[] messageBuffer = new byte[4096];

                int receiveBytes = connection.Receive(messageBuffer);

                string message = Encoding.UTF8.GetString(messageBuffer, 0, receiveBytes);

                return message;
            }
            catch (SocketException ex)
            {
                if (ex.SocketErrorCode == SocketError.ConnectionReset)
                {
                }
            }
            catch (Exception ex)
            {

            }

            return "";
        }

        private void ListenToMessages(Socket connection)
        {
            while (true)
            {
                if (!connection.Connected)
                    break;

                string message = ListenToMessage(connection);

                if (!string.IsNullOrEmpty(message))
                    SendMessage(message);
            }
        }

        private Task ListenToAsync(Socket connection)
        {
            return Task.Run(() => ListenToMessages(connection));
        }
    }
}
/*Написать полноценный чат. 
Клиент должен указать в настройках ip/name pc и порт сервера, 
который должен организовать прием и пересылку сообщений клиентов. 
У клиента должен отображаться список пользователей online. 
Клиент должнен передавать и принимать пока что только текстовые сообщения другим пользователям чата
(клиентам).
Обязательно пусть не ослепительный, но нормальный интерфейс + соблюдение правил кодирования.*/